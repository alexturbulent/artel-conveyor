export const data = [
    {
        conveyor_id: 1,
        title: 'First',
        positions: [
            {
                id: 100,
                employee_id: 'uuid',
                employee_label: 'Anton',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 101,
                employee_id: 'uuid',
                employee_label: 'Anton',
                status_id: 2,
                status_label: 'active',
            },
            {
                id: 102,
                employee_id: 'uuid',
                employee_label: 'Anton',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 103,
                employee_id: 'uuid',
                employee_label: 'Anton',
                status_id: 3,
                status_label: 'active',
            },
            {
                id: 104,
                employee_id: 'uuid',
                employee_label: 'Anton',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 105,
                employee_id: 'uuid',
                employee_label: 'Anton',
                status_id: 1,
                status_label: 'active',
            },
        ]
    },
    {
        conveyor_id: 2,
        title: 'Second',
        positions: [
            {
                id: 200,
                employee_id: 'uuid',
                employee_label: 'Parton',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 201,
                employee_id: 'uuid',
                employee_label: 'Parton',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 202,
                employee_id: 'uuid',
                employee_label: 'Parton',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 203,
                employee_id: 'uuid',
                employee_label: 'Parton',
                status_id: 1,
                status_label: 'active',
            },
        ]
    },
    {
        conveyor_id: 3,
        title: 'Third',
        positions: [
            {
                id: 300,
                employee_id: 'uuid',
                employee_label: 'Aramis',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 301,
                employee_id: 'uuid',
                employee_label: 'Aramis',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 302,
                employee_id: 'uuid',
                employee_label: 'Aramis',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 303,
                employee_id: 'uuid',
                employee_label: 'Aramis',
                status_id: 1,
                status_label: 'active',
            },
        ]
    },
    {
        conveyor_id: 4,
        title: 'Fourth',
        positions: [
            {
                id: 400,
                employee_id: 'uuid',
                employee_label: 'Dartanyan`',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 401,
                employee_id: 'uuid',
                employee_label: 'Dartanyan',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 402,
                employee_id: 'uuid',
                employee_label: 'Dartanyan',
                status_id: 1,
                status_label: 'active',
            },
            {
                id: 403,
                employee_id: 'uuid',
                employee_label: 'Dartanyan',
                status_id: 1,
                status_label: 'active',
            },
        ]
    },
]