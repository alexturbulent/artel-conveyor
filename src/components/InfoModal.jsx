import React from "react";
import { Modal } from 'antd';

const ModalContent = (props) => {
    if (props.obj) {
        const list = Object.entries(props.obj);
        return list.map((item) => (
            <p key={item[0]}>
                <b>{item[0]}</b>: {item[1]}
            </p>
        ))
    } else {
        return null;
    }
}

const InfoModal = (props) => {
    return (
        <Modal
            title="Position info"
            visible={props.visible}
            onCancel={props.handleCancel}
            footer={null}
            centered
        >
            <ModalContent obj={props.data} />
        </Modal>
    )
}

export default InfoModal;