import React from 'react';
import {
    Layout,
    Breadcrumb,
    Menu,
    Card,
    Empty,
} from 'antd';
import InfoModal from "./components/InfoModal";
import { data } from "./data";
import './App.css';

const { Header, Content } = Layout;

class App extends React.Component {

    constructor() {
        super();
        this.state = {
            cardWidth: 5,
            isInfoModalOpen: false,
            modalObj: null,
            data: [],
        }
    }

    openInfoModal = (obj) => {
        this.setState({
            isInfoModalOpen: true,
            modalObj: obj,
        })
    }

    closeInfoModal = () => this.setState({
        isInfoModalOpen: false,
        modalObj: null,
    })

    checkStatus = (id) => {
        switch (id) {
            case 1:
                return "position-green";
            case 2:
                return "position-orange";
            case 3:
                return "position-red";

            default:
                return "position-red"
        }
    }

    componentDidMount() {
        if (data && data.length && Array.isArray(data)) {
            let refactoredData = [];
            let isAnyPositionEmpty = false;
            let positionsLengthList = [];

            data.forEach((conveyor) => {
                if (conveyor && conveyor.positions && conveyor.positions.length && Array.isArray(conveyor.positions)) {
                    conveyor.positions.forEach((position) => {
                        if (isAnyPositionEmpty) {
                            position.className = this.checkStatus(3);
                        } else if (position.status_id === 3) {
                            position.className = this.checkStatus(position.status_id);
                            isAnyPositionEmpty = true;
                        } else {
                            position.className = this.checkStatus(position.status_id);
                        }
                    })
                }
                positionsLengthList.push(conveyor.positions.length >= 0 ? conveyor.positions.length : 0);
                isAnyPositionEmpty = false;
                refactoredData.push(conveyor);
            })

            const maxPositionLength = Math.max(...positionsLengthList);

            this.setState({
                data: refactoredData,
                cardWidth: maxPositionLength < 30 ? 100 / maxPositionLength : 5,
            })
        }
    }

    render() {
        const {
            cardWidth,
            isInfoModalOpen,
            modalObj,
            data,
        } = this.state;

        return (
            <React.Fragment>
                <Layout>
                    <Header>
                        <Menu
                            theme="dark"
                            mode="horizontal"
                            defaultSelectedKeys={['conveyor']}
                            style={{ lineHeight: '64px' }}
                        >
                            <Menu.Item key="conveyor">Conveyor</Menu.Item>
                            <Menu.Item key="audit">Audit</Menu.Item>
                        </Menu>
                    </Header>
                    <Content style={{ padding: '0 50px' }}>
                        <Breadcrumb style={{ margin: '16px 0' }}>
                            <Breadcrumb.Item>Home</Breadcrumb.Item>
                            <Breadcrumb.Item>Conveyor</Breadcrumb.Item>
                        </Breadcrumb>
                        <div className="site-layout-content">
                            {
                                data && data.length ? data.map((conveyor) => (
                                    <Card title={conveyor.title} key={conveyor.conveyor_id} className="conveyor">
                                        {
                                            conveyor && conveyor.positions && conveyor.positions.length ? conveyor.positions.map((position, index) => (
                                                <Card.Grid
                                                    className={`position-card ${position.className}`}
                                                    key={position.id}
                                                    style={{
                                                        width: `${cardWidth}%`
                                                    }}
                                                    onClick={() => {
                                                        this.openInfoModal(position)
                                                    }}
                                                >
                                                    {index + 1}
                                                    {/* {position.employee_label} */}
                                                </Card.Grid>
                                            )) : null
                                        }
                                    </Card>
                                )) : <Empty />
                            }
                        </div>
                    </Content>
                </Layout>
                <InfoModal visible={isInfoModalOpen} handleCancel={this.closeInfoModal} data={modalObj} />
            </React.Fragment>
        );
    }
}

export default App;
